#include QMK_KEYBOARD_H

// Tap Dance declarations
enum {
    TD_NUM_RGB,
};

// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
    // Tap once for NumLock, twice for RGB layer
    [TD_NUM_RGB] = ACTION_TAP_DANCE_LAYER_TOGGLE(KC_NUM, 1),
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
     * ┌───┬───┬───┬───┐
     * │ ⏮ │ ⏯ │ ■ │ ⏭ │
     * ├───┼───┼───┼───┤
     * │Num│ / │ * │ - │
     * ├───┼───┼───┼───┤
     * │ 7 │ 8 │ 9 │   │
     * ├───┼───┼───┤ + │
     * │ 4 │ 5 │ 6 │   │
     * ├───┼───┼───┼───┤
     * │ 1 │ 2 │ 3 │   │
     * ├───┴───┼───┤Ent│
     * │   0   │ . │   │
     * └───────┴───┴───┘
     */
    [0] = LAYOUT_numpad_6x4(
        KC_MPRV,         KC_MPLY, KC_MSTP, KC_MNXT,
        TD(TD_NUM_RGB), KC_PSLS, KC_PAST, KC_PMNS,
        KC_P7,           KC_P8,   KC_P9,
        KC_P4,           KC_P5,   KC_P6,   KC_PPLS,
        KC_P1,           KC_P2,   KC_P3,
        KC_P0,                    KC_PDOT, KC_PENT
    ),

    /*
     * ┌─────┬─────┬─────┬─────┐
     * │ModeR│Breat│Plain│Mode │
     * ├─────┼─────┼─────┼─────┤
     * │     │Rainb│Swirl│Sped-│
     * ├─────┼─────┼─────┼─────┤
     * │Brig+│Brig-│     │     │
     * ├─────┼─────┼─────┤Sped+│
     * │Satu+│Satu-│     │     │
     * ├─────┼─────┼─────┼─────┤
     * │Hue+ │Hue- │     │     │
     * ├─────┴─────┼─────┤OnOff│
     * │           │     │     │
     * └───────────┴─────┴─────┘
     */
    [1] = LAYOUT_numpad_6x4(
        RGB_RMOD, RGB_M_B, RGB_M_P,  RGB_MOD,
        _______,  RGB_M_R, RGB_M_SW, RGB_SPD,
        RGB_VAI,  RGB_VAD, _______,
        RGB_SAI,  RGB_SAD, _______,  RGB_SPI,
        RGB_HUI,  RGB_HUD, _______,
        _______,           _______,  RGB_TOG
    )
};

