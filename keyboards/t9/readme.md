# t9

![t9](https://i.imgur.com/NJGw3d7.png)

A QMK based 6x4 numpad keyboard intended to act as a switch and keycap tester.

* Keyboard Maintainer: [XenGi](https://github.com/XenGi)
* Hardware Supported: [t9](https://github.com/tnine/pcb)
* Hardware Availability: [gerber files](https://gitlab.com/tnine/pcb/-/releases/rev1)

Make example for this keyboard (after setting up your build environment):

```shell
make t9:default
# or with the QMK tool
qmk compile -kb t9 -km default
```

Flashing example for this keyboard:

```shell
make t9:default:flash
# or with the QMK tool
qmk flash -kb t9 -km default
```

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Bootloader

Enter the bootloader in 3 ways:

* **Bootmagic reset**: Hold down the key at (0,0) in the matrix (usually the top left key or Escape) and plug in the keyboard
* **Physical reset button**: Briefly short the pads on the back of the PCB
