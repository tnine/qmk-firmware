#include "quantum.h"

led_config_t g_led_config = { {
  // Key Matrix to LED Index
  { 20,     19, 18, 17,    },
  { 16,     15, 14, 13,    },
  { 12,     11, 10, NO_LED },
  {  9,      8,  7, 6      },
  {  5,      4,  3, NO_LED },
  {  2, NO_LED,  1, 0      }
}, {
  // LED Index to Physical Position
  // x = 224 / (NUMBER_OF_COLS - 1) * COL_POSITION
  // y =  64 / (NUMBER_OF_ROWS - 1) * ROW_POSITION
  { 224, 58 }, { 149, 64 }, { 37, 64 }, { 149, 51 }, { 75, 51 }, { 0, 51 }, { 224, 32 }, { 149, 38 }, { 75, 38 }, { 0, 38 }, { 149, 26 }, { 75, 26 }, { 0, 26 }, { 224, 13 }, { 149, 13 }, { 75, 13 }, { 0, 13 }, { 224, 0 }, { 149, 0 }, { 75, 0 }, { 0, 0 }
}, {
  // LED Index to Flag
  4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
} };

