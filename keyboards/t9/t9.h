#pragma once

#include "quantum.h"

#define ___ KC_NO

#define LAYOUT_numpad_6x4( \
    k00, k01, k02, k03, \
    k10, k11, k12, k13, \
    k20, k21, k22,      \
    k30, k31, k32, k33, \
    k40, k41, k42,      \
    k50,      k52, k53  \
) { \
    { k00, k01, k02, k03 }, \
    { k10, k11, k12, k13 }, \
    { k20, k21, k22, ___ }, \
    { k30, k31, k32, k33 }, \
    { k40, k41, k42, ___ }, \
    { k50, ___, k52, k53 } \
}

